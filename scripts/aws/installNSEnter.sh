#!/bin/bash
# FROM: http://www.sebastien-han.fr/blog/2014/01/27/access-a-container-without-ssh/

#apt-get install -y make gcc
yum install -y make gcc

wget https://www.kernel.org/pub/linux/utils/util-linux/v2.24/util-linux-2.24.tar.bz2
mv util-linux-2.24.tar.bz2 tmp/
cd tmp
bzip2 -d -c util-linux-2.24.tar.bz2 | tar xvf -
cd util-linux-2.24/
./configure --without-ncurses
make nsenter
cp nsenter /usr/local/bin
