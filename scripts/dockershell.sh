#!/bin/bash

# Check to see if the docker name was provided
if [ $# -eq 0 ]
  then
    echo "ERROR: no dockername given as argument"
    exit 0
fi

PID=$(docker inspect --format {{.State.Pid}} $1)
nsenter --target $PID --mount --uts --ipc --net --pid
